class ItemsMailer < ActionMailer::Base
  default from: 'info@mystore.com',
          template_path: 'mailers/items'

  def item_destroyed(item)
    @item = item
    mail to: 'gravisbesya@list.ru',
         subject: 'Item destroyed'
  end

end
