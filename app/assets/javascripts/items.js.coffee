# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ($) ->
  $('.deleteAction').click ->
    return false unless confirm 'Are you sure?'
    current_item_tr = $(this).parents('tr')[0]
    $.ajax
      url: '/items/' + $(current_item_tr).attr('data-item-id')
      type: 'POST'
      data:
        _method: 'DELETE'
      success: (result) ->
        $(current_item_tr).fadeOut(200)
        console.log result