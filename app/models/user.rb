class User < ActiveRecord::Base
  has_one   :cart
  has_many  :orders
  has_many  :comments


  before_destroy { self.cart.destroy }

end