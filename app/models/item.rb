class Item < ActiveRecord::Base
  validates :price, numericality: { greater_than: 0, allow_nil: true }
  validates :name, :description, presence: true

  has_many  :positions
  has_many  :carts, through: :positions
  has_many  :comments, as: :commentable
  has_one   :image, as: :imageable
  has_and_belongs_to_many :orders


  def image=(new_image)
    if !self.image || new_record?
      @image = Image.create({file: new_image, imageable: self})
    elsif self.image
      self.image.update_attributes({file: new_image})
    end
  end
end
