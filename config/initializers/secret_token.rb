# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyStore::Application.config.secret_key_base = 'fc0af49740dbbce024a851042291622aaab58e7bdfc72af4eb54b56d52dffad40451e901ec97e1eb5e4dd253815edd1dd46907def1f9ad2f1e97e897faa1282a'
