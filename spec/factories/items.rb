FactoryGirl.define do
  factory :item do
    price 10
    sequence(:name) { |i| "Item #{i}" }
    sequence(:description) { |i| "Item #{i} description" }
    real true
    weight 10
  end
end