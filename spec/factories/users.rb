FactoryGirl.define do
  factory :user do
    sequence(:login) { |i| "Login #{i}" }
  end
end